using System;
using Xunit;
using RpgCharacters.Character;

namespace RpgCharactersTests
{
    public class CharacterTests
    {
        [Fact]
        public void Character_CreateCharacter_LevelShouldBeOne()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            int expectedValue = 1;
            // ACT
            int returnValue = warrior.GetLevel();
            // ASSERT
            Assert.Equal(expectedValue, returnValue);
        }

        [Fact]
        public void LevelUp_LevelUpCharacter_LevelShouldBeTwo()
        {
            // ARRANGE
            Mage mage = new("Mage");
            int insertValue = 1;
            int expetectValue = 2;
            // ACT
            mage.LevelUp(insertValue);
            int returnValue = mage.GetLevel();
            // ASSERT
            Assert.Equal(expetectValue, returnValue);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_LevelUpWithNumberLessThanOne_ArgumentExceptionShouldBeThrown(int data)
        {
            // ARRANGE
            Ranger ranger = new("Ranger");
            // ACT & ASSERT
            Assert.Throws<ArgumentException>(() => ranger.LevelUp(data));
        }

        [Fact]
        public void Warrior_CheckStartAttributes_ShouldBeStartAttributes()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            int expectedStr = 5;
            int expectedDex = 2;
            int expectedInt = 1;
            int expectedVit = 10;
            // ACT
            PrimaryAttributes expectedPrimaryAttributes = new() { Strength = expectedStr, Dexterity = expectedDex, Intelligence = expectedInt, Vitality = expectedVit };
            PrimaryAttributes actualPrimaryAttributes = new() { Strength = warrior.BasePrimaryAttributes.Strength, Dexterity = warrior.BasePrimaryAttributes.Dexterity, Intelligence = warrior.BasePrimaryAttributes.Intelligence, Vitality = warrior.BasePrimaryAttributes.Vitality };
            // ASSERT
            Assert.Equal(expectedPrimaryAttributes, actualPrimaryAttributes);
        }

        [Fact]
        public void Mage_CheckStartAttributes_ShouldBeStartAttributes()
        {
            // ARRANGE
            Mage mage = new("Mage");
            int expectedStr = 1;
            int expectedDex = 1;
            int expectedInt = 8;
            int expectedVit = 5;
            // ACT
            PrimaryAttributes expectedPrimaryAttributes = new() { Strength = expectedStr, Dexterity = expectedDex, Intelligence = expectedInt, Vitality = expectedVit };
            PrimaryAttributes actualPrimaryAttributes = new() { Strength = mage.BasePrimaryAttributes.Strength, Dexterity = mage.BasePrimaryAttributes.Dexterity, Intelligence = mage.BasePrimaryAttributes.Intelligence, Vitality = mage.BasePrimaryAttributes.Vitality };
            // ASSERT
            Assert.Equal(expectedPrimaryAttributes, actualPrimaryAttributes);
        }

        [Fact]
        public void Ranger_CheckStartAttributes_ShouldBeStartAttributes()
        {
            // ARRANGE
            Ranger ranger = new("Ranger");
            int expectedStr = 1;
            int expectedDex = 7;
            int expectedInt = 1;
            int expectedVit = 8;
            // ACT
            PrimaryAttributes expectedPrimaryAttributes = new() { Strength = expectedStr, Dexterity = expectedDex, Intelligence = expectedInt, Vitality = expectedVit };
            PrimaryAttributes actualPrimaryAttributes = new() { Strength = ranger.BasePrimaryAttributes.Strength, Dexterity = ranger.BasePrimaryAttributes.Dexterity, Intelligence = ranger.BasePrimaryAttributes.Intelligence, Vitality = ranger.BasePrimaryAttributes.Vitality };
            // ASSERT
            Assert.Equal(expectedPrimaryAttributes, actualPrimaryAttributes);
        }

        [Fact]
        public void Rogue_CheckStartAttributes_ShouldBeStartAttributes()
        {
            // ARRANGE
            Rogue rogue = new("Rogue");
            int expectedStr = 2;
            int expectedDex = 6;
            int expectedInt = 1;
            int expectedVit = 8;
            // ACT
            PrimaryAttributes expectedPrimaryAttributes = new() { Strength = expectedStr, Dexterity = expectedDex, Intelligence = expectedInt, Vitality = expectedVit };
            PrimaryAttributes actualPrimaryAttributes = new() { Strength = rogue.BasePrimaryAttributes.Strength, Dexterity = rogue.BasePrimaryAttributes.Dexterity, Intelligence = rogue.BasePrimaryAttributes.Intelligence, Vitality = rogue.BasePrimaryAttributes.Vitality };
            // ASSERT
            Assert.Equal(expectedPrimaryAttributes, actualPrimaryAttributes);
        }

        [Fact]
        public void LevelUp_WarriorIncreaseBaseAttributes_ShouldIncreasePrimaryAttributesAsExpected()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            int expectedStr = 8;
            int expectedDex = 4;
            int expectedInt = 2;
            int expectedVit = 15;
            // ACT
            warrior.LevelUp(1);
            PrimaryAttributes expectedPrimaryAttributes = new() { Strength = expectedStr, Dexterity = expectedDex, Intelligence = expectedInt, Vitality = expectedVit };
            PrimaryAttributes actualPrimaryAttributes = new() { Strength = warrior.BasePrimaryAttributes.Strength, Dexterity = warrior.BasePrimaryAttributes.Dexterity, Intelligence = warrior.BasePrimaryAttributes.Intelligence, Vitality = warrior.BasePrimaryAttributes.Vitality };
            // ASSERT
            Assert.Equal(expectedPrimaryAttributes, actualPrimaryAttributes);
        }

        [Fact]
        public void LevelUp_MageIncreaseBaseAttributes_ShouldIncreasePrimaryAttributesAsExpected()
        {
            // ARRANGE
            Mage mage = new("Mage");
            int expectedStr = 2;
            int expectedDex = 2;
            int expectedInt = 13;
            int expectedVit = 8;
            // ACT
            mage.LevelUp(1);
            PrimaryAttributes expectedPrimaryAttributes = new() { Strength = expectedStr, Dexterity = expectedDex, Intelligence = expectedInt, Vitality = expectedVit };
            PrimaryAttributes actualPrimaryAttributes = new() { Strength = mage.BasePrimaryAttributes.Strength, Dexterity = mage.BasePrimaryAttributes.Dexterity, Intelligence = mage.BasePrimaryAttributes.Intelligence, Vitality = mage.BasePrimaryAttributes.Vitality };
            // ASSERT
            Assert.Equal(expectedPrimaryAttributes, actualPrimaryAttributes);
        }

        [Fact]
        public void LevelUp_RangerIncreaseBaseAttributes_ShouldIncreasePrimaryAttributesAsExpected()
        {
            // ARRANGE
            Ranger ranger = new("Ranger");
            int expectedStr = 2;
            int expectedDex = 12;
            int expectedInt = 2;
            int expectedVit = 10;
            // ACT
            ranger.LevelUp(1);
            PrimaryAttributes expectedPrimaryAttributes = new() { Strength = expectedStr, Dexterity = expectedDex, Intelligence = expectedInt, Vitality = expectedVit };
            PrimaryAttributes actualPrimaryAttributes = new() { Strength = ranger.BasePrimaryAttributes.Strength, Dexterity = ranger.BasePrimaryAttributes.Dexterity, Intelligence = ranger.BasePrimaryAttributes.Intelligence, Vitality = ranger.BasePrimaryAttributes.Vitality };
            // ASSERT
            Assert.Equal(expectedPrimaryAttributes, actualPrimaryAttributes);
        }

        [Fact]
        public void LevelUp_RogueIncreaseBaseAttributes_ShouldIncreasePrimaryAttributesAsExpected()
        {
            // ARRANGE
            Rogue rogue = new("Rogue");
            int expectedStr = 3;
            int expectedDex = 10;
            int expectedInt = 2;
            int expectedVit = 11;
            // ACT
            rogue.LevelUp(1);
            PrimaryAttributes expectedPrimaryAttributes = new() { Strength = expectedStr, Dexterity = expectedDex, Intelligence = expectedInt, Vitality = expectedVit };
            PrimaryAttributes actualPrimaryAttributes = new() { Strength = rogue.BasePrimaryAttributes.Strength, Dexterity = rogue.BasePrimaryAttributes.Dexterity, Intelligence = rogue.BasePrimaryAttributes.Intelligence, Vitality = rogue.BasePrimaryAttributes.Vitality };
            // ASSERT
            Assert.Equal(expectedPrimaryAttributes, actualPrimaryAttributes);
        }

        [Fact]
        public void LevelUp_CalculateSecondaryAttributes_ShouldIncreaseSecondaryAttributesAsExpected()
        {
            // ARRANGE
            Warrior warrior = new("Warrior");
            int expectedHealth = 150;
            int expectedArmorRating = 12;
            int expectedElementalResistance = 2;
            // ACT
            warrior.LevelUp(1);
            SecondaryAttributes expectedSecondaryAttributes = new() { Health = expectedHealth, ArmorRating = expectedArmorRating, ElementalResistance = expectedElementalResistance };
            SecondaryAttributes actualSecondaryAttributes = new() { Health = warrior.SecondaryAttributes.Health, ArmorRating = warrior.SecondaryAttributes.ArmorRating, ElementalResistance = warrior.SecondaryAttributes.ElementalResistance };
            // ASSERT
            Assert.Equal(expectedSecondaryAttributes, actualSecondaryAttributes);
        }
    }
}
