﻿using RpgCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgCharacters.Interfaces
{
    interface IEquip
    {
        public string Equip(Armor a);
        public string Equip(Weapon w);
    }
}
